export type SongType = {
  title: string;
  chords: {[key: string | number]: string[][] | undefined};
  yt: string;
};

export type Chords = {
  [key: string | number]: string[] | undefined;
};

export type Chord =
  | 'C'
  | 'Cis'
  | 'D'
  | 'Dis'
  | 'E'
  | 'F'
  | 'Fis'
  | 'G'
  | 'Gis'
  | 'A'
  | 'Ais'
  | 'H'
  | 'C7'
  | 'Cis7'
  | 'D7'
  | 'Dis7'
  | 'E7'
  | 'F7'
  | 'Fis7'
  | 'G7'
  | 'Gis7'
  | 'A7'
  | 'Ais7'
  | 'H7'
  | 'c'
  | 'cis'
  | 'd'
  | 'dis'
  | 'e'
  | 'f'
  | 'fis'
  | 'g'
  | 'gis'
  | 'a'
  | 'ais'
  | 'h'
  | 'c7'
  | 'cis7'
  | 'd7'
  | 'dis7'
  | 'e7'
  | 'f7'
  | 'fis7'
  | 'g7'
  | 'gis7'
  | 'a7'
  | 'ais7'
  | 'h7';
