import React, {useState, useEffect} from 'react';
import * as eva from '@eva-design/eva';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  ImageBackground,
  Dimensions,
  StyleSheet,
  View,
} from 'react-native';
import {
  Text,
  ApplicationProvider,
  IconRegistry,
  RadioGroup,
  Radio,
  Button,
} from '@ui-kitten/components';
import songs from './songs';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import AnimatedSplash from 'react-native-animated-splash-screen';
import logo from './assets/images/logo.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import background from './assets/images/background.jpg';
import SongComponent from './components/SongComponent';
import SongBottomComponent from './components/SongBottomComponent';
import SideMenu from 'react-native-side-menu-updated';
import EditSongComponent from './components/EditSongComponent';
import Toast from 'react-native-simple-toast';

import {SongType} from './types';

const App = () => {
  const [value, setValue] = useState(null);
  const [data, setData] = useState<null | SongType[]>(null);
  const [song, setSong] = useState<null | undefined | SongType>(null);
  const [tonNumber, setTonNumber] = useState<number>(0);
  const [checked, setChecked] = useState<boolean>(false);
  const [isLoading, setLoading] = useState<boolean>(false);
  const [tab, setTab] = useState<number>(0);

  useEffect(() => {
    setTimeout(() => {
      setLoading(true);
    }, 300);
    getData();
  }, []);

  useEffect(() => {
    value && data && setSong(data.find(element => element.title === value));
  }, [value, data]);

  useEffect(() => {
    data && storeData(data);
  }, [data]);

  const storeData = async (value1: SongType[]) => {
    try {
      const jsonValue = JSON.stringify(value1);
      await AsyncStorage.setItem('songs', jsonValue);
    } catch (e) {
      console.log('error: ', e);
    }
  };

  const getData = async () => {
    try {
      const value2 = await AsyncStorage.getItem('songs');
      if (value2 !== null) {
        setData(JSON.parse(value2));
      }
    } catch (e) {
      console.log('error: ', e);
    }
  };

  const LeftMenu = (
    <View style={styles.leftMenuView}>
      <Text style={styles.settings} category="h6">
        Ustawienia
      </Text>
      <RadioGroup selectedIndex={tab} onChange={index => setTab(index)}>
        <Radio>Pieśni</Radio>
        <Radio>Nowa Pieśń</Radio>
        <Radio>Edycja</Radio>
      </RadioGroup>
      <Button
        appearance="outline"
        style={styles.addSongsButton}
        onPress={() => {
          setData(songs);
          Toast.show('Dodano pieśni');
        }}>
        Dodaj pieśni podstawowe
      </Button>
      <Button
        appearance="outline"
        style={styles.addSongsButton}
        status="danger"
        onPress={() => {
          setData([]);
          Toast.show('Usunięto pieśni');
        }}>
        Usuń wszystkie pieśni
      </Button>
    </View>
  );

  return (
    <>
      <IconRegistry icons={EvaIconsPack} />
      <ApplicationProvider {...eva} theme={eva.dark}>
        <AnimatedSplash
          translucent={true}
          isLoaded={isLoading}
          logoImage={logo}
          backgroundColor={'#000'}
          logoHeight={150}
          logoWidth={150}>
          <SideMenu menu={LeftMenu} autoClosing={false}>
            <SafeAreaView>
              <StatusBar barStyle={'light-content'} />
              <ImageBackground
                source={background}
                style={{
                  width: Dimensions.get('window').width,
                  height: Dimensions.get('window').height,
                }}>
                <ScrollView
                  contentInsetAdjustmentBehavior="automatic"
                  style={styles.scrollView}>
                  <Text category="h3" style={styles.mainHeader}>
                    Chords manager
                  </Text>
                  {tab === 0 ? (
                    <SongComponent
                      setValue={setValue}
                      data={data}
                      tonNumber={tonNumber}
                      setData={setData}
                      value={value}
                      setSong={setSong}
                      song={song}
                      checked={checked}
                    />
                  ) : (
                    <EditSongComponent
                      songs={data}
                      setSongs={setData}
                      tab={tab}
                    />
                  )}
                </ScrollView>
              </ImageBackground>
              {tab === 0 && song && (
                <SongBottomComponent
                  song={song}
                  setTonNumber={setTonNumber}
                  tonNumber={tonNumber}
                  checked={checked}
                  setChecked={setChecked}
                />
              )}
            </SafeAreaView>
          </SideMenu>
        </AnimatedSplash>
      </ApplicationProvider>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    padding: 20,
  },
  mainHeader: {
    marginBottom: 10,
    fontFamily: 'Rancho-Regular',
    color: 'white',
  },
  leftMenuView: {
    backgroundColor: 'rgba(0,0,0,1)',
    height: '100%',
    padding: 30,
  },
  settings: {
    marginBottom: 20,
  },
  addSongsButton: {
    borderColor: 'rgba(200,200,200,0.3)',
    marginTop: 100,
  },
});

export default App;
