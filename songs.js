const songs = [
  {
    title: 'Stoję dziś',
    chords: {
      1: [
        ['A', 'E', 'H', 'cis'],
        ['A', 'E', 'H', 'cis'],
        ['A', 'E', 'H'],
      ],
    },
    yt: 'jlc7rWVvq2Q',
  },
  {
    title: 'Twoje ciało jest prawdziwym pokarmem',
    chords: {
      1: [
        ['d', 'C', 'd'],
        ['g', 'C', 'd'],
        ['g', 'd', 'C', 'd', 'C', 'd', 'C', 'd'],
      ],
    },
    yt: '5F1hEDgSIc4',
  },
  {
    title: 'O panie Ty nam dajesz',
    chords: {
      1: [
        ['h', 'D', 'e', 'Fis7', 'h'],
        ['G', 'A', 'e', 'D', 'G', 'A', 'Fis'],
        ['G', 'A', 'D', 'h', 'e', 'Fis7', 'h'],
      ],
    },
    yt: '5EMaGBPFwgA',
  },
  {
    title: 'Niechaj Cię panie',
    chords: {
      1: [
        ['e', 'h', 'G', 'A', 'H'],
        ['e', 'A', 'H'],
        ['G', 'A', 'h', 'G', 'e', 'A', 'H'],
      ],
    },
    yt: 'ayRbPpvd2Rk',
  },
  {
    title: 'Śpiewajmy Mu, dziękujmy Mu',
    chords: {
      1: [['E'], ['A', 'E'], ['H', 'A']],
      2: [
        ['E', 'A'],
        ['E', 'A'],
      ],
    },
    yt: 'g3U6_ZDa4RE',
  },
  {
    title: 'Jezu cichy',
    chords: {
      1: [
        ['e', 'C', 'G', 'D'],
        ['e', 'C', 'G', 'H7'],
        ['D', 'A'],
        ['e', 'G', 'A'],
        ['e', 'C', 'G', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Łaską jesteśmy zbawieni',
    chords: {
      1: [
        ['A', 'fis', 'D', 'E'],
        ['cis', 'fis', 'D', 'E'],
      ],
    },
    yt: '',
  },
  {
    title: 'Chlebie najcichszy',
    chords: {
      1: [['G', 'h', 'C', 'D', 'G', 'a', 'e']],
      2: [['e', 'C', 'G', 'D']],
    },
    yt: '',
  },
  {
    title: 'Ty światłość dnia',
    chords: {
      1: [['D', 'A', 'G']],
      2: [['D', 'A', 'h', 'A', 'G']],
    },
    yt: '',
  },
  {
    title: 'Pan mnie strzeże',
    chords: {
      1: [['C', 'a', 'e', 'F', 'G']],
    },
    yt: '',
  },
  {
    title: 'Chwała Bogu Ojcu',
    chords: {
      1: [
        ['e', 'D', 'e', 'D'],
        ['e', 'D', 'e', 'D'],
      ],
      2: [['e', 'D', 'D', 'h', 'e']],
    },
    yt: '',
  },
  {
    title: 'Tam gdzie Twój tron',
    chords: {
      1: [['D', 'A', 'h', 'G']],
    },
    yt: '',
  },
  {
    title: 'Skosztujcie i zobaczcie',
    chords: {
      1: [
        ['e', 'a', 'C'],
        ['G', 'D'],
      ],
      2: [
        ['e', 'h', 'a', 'C', 'D'],
        ['e', 'C', 'G', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Przed tronem Twym',
    chords: {
      1: [
        ['C', 'B', 'F', 'C', 'B', 'F'],
        ['C', 'B', 'F', 'C', 'B', 'G', 'C'],
      ],
      2: [
        ['F', 'B', 'd', 'C'],
        ['F', 'B', 'F'],
      ],
    },
    yt: '',
  },
  {
    title: 'Boże twa łaska',
    chords: {
      1: [
        ['D', 'A', 'G', 'D', 'A', 'G'],
        ['e', 'D', 'A', 'e', 'h', 'D', 'A'],
      ],
      2: [['D', 'G', 'A']],
    },
    yt: '',
  },
  {
    title: 'Szukajcie wpierw Królestwa Bożego',
    chords: {
      1: [
        ['C', 'G', 'a', 'e', 'F', 'C', 'G'],
        ['C', 'G', 'a', 'e', 'F', 'C', 'G', 'C'],
      ],
    },
    yt: '',
  },
  {
    title: 'Nie zabraknie mi nigdy chleba',
    chords: {
      1: [
        ['C', 'G', 'F', 'C'],
        ['F', 'C', 'F', 'G', 'C', 'G'],
      ],
    },
    yt: '',
  },
  {
    title: 'Wodę zamieniłeś w wino',
    chords: {
      1: [
        ['h', 'G', 'D', 'h', 'G', 'D'],
        ['e', 'A'],
      ],
      2: [['h', 'G', 'D', 'A']],
      3: [
        ['h', 'G'],
        ['D', 'A'],
      ],
    },
    yt: '',
  },
  {
    title: 'O wychwalajcie Go wszystkie narody',
    chords: {
      1: [['D', 'G', 'A']],
    },
    yt: '',
  },
  {
    title: 'Twoja miłość jest pierwsza',
    chords: {
      1: [
        ['D', 'A'],
        ['h', 'G'],
      ],
    },
    yt: '',
  },
  {
    title: 'Rozpięty na ramionach',
    chords: {
      1: [['a', 'C', 'G', 'F', 'a', 'E']],
      2: [['a', 'F', 'G', 'a', 'F', 'G', 'h', 'E7']],
    },
    yt: '',
  },
  {
    title: 'Kto spożywa moje ciało',
    chords: {
      1: [
        ['d', 'a', 'g', 'A'],
        ['d', 'a', 'g', 'A'],
        ['F', 'C', 'g', 'C7', 'C'],
        ['F', 'C', 'g', 'A'],
      ],
    },
    yt: '',
  },
  {
    title: 'Chlebie żywy, łamany drzewem krzyża',
    chords: {
      1: [
        ['d', 'C', 'B', 'A'],
        ['B', 'A', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Nic nie musisz mówić nic',
    chords: {
      1: [['d', 'B', 'C', 'F', 'A']],
    },
    yt: '',
  },
  {
    title: 'Nasz Pan na pewno przyjdzie',
    chords: {
      1: [['A', 'E', 'fis', 'D', 'E']],
    },
    yt: '',
  },
  {
    title: 'Przyjdź Jezu, przyjdź',
    chords: {
      1: [
        ['D', 'G', 'h', 'A'],
        ['e', 'h', 'e', 'h'],
        ['e', 'h', 'A'],
        ['G', 'D', 'A'],
      ],
    },
    yt: '',
  },
  {
    title: 'Czekam na Ciebie, Jezu mój mały',
    chords: {
      1: [
        ['h', 'D', 'G', 'D', 'A'],
        ['e', 'h', 'e', 'Fis', 'h'],
      ],
      2: [
        ['Fis', 'h', 'e', 'A', 'D'],
        ['Fis', 'h'],
        ['e', 'G', 'Fis', 'h'],
      ],
    },
    yt: '',
  },
  {
    title: 'Witaj Gwiazdo morza',
    chords: {
      1: [
        ['D', 'G', 'A', 'D'],
        ['e', 'A', 'fis'],
        ['h', 'G', 'A', 'D'],
        ['e', 'G', 'A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Zamrtwychwstał Pan',
    chords: {
      1: [
        ['a', 'F', 'C', 'G'],
        ['F', 'a', 'C', 'G'],
      ],
      2: [['a', 'G', 'C', 'd', 'E7']],
    },
    yt: '',
  },
  {
    title: 'Wszyscy ludzie klaszczą w dłonie',
    chords: {
      1: [['C', 'G', 'D', 'e']],
      2: [
        ['C', 'D', 'C', 'D', 'e'],
        ['C', 'G', 'D', 'C', 'G', 'D', 'e'],
      ],
    },
    yt: '',
  },
  {
    title: 'Gdy drogi pomyli los zły',
    chords: {
      1: [
        ['F', 'B', 'C', 'd'],
        ['B', 'C', 'F'],
        ['F', 'B', 'C', 'd'],
        ['B', 'C', 'F', 'F7'],
      ],
      2: [
        ['B', 'C', 'F', 'C', 'd'],
        ['B', 'C', 'F'],
      ],
    },
    yt: '',
  },
  {
    title: 'Przyjdźcie do stołu miłosierdzia',
    chords: {
      1: [
        ['F', 'B', 'C', 'd'],
        ['B', 'C', 'F'],
        ['F', 'B', 'C', 'd'],
        ['B', 'C', 'F', 'F7'],
      ],
      2: [
        ['B', 'C', 'F', 'C', 'd'],
        ['B', 'C', 'F'],
      ],
    },
    yt: '',
  },
  {
    title: 'Zwycięzca śmierci',
    chords: {
      1: [
        ['D', 'A', 'D', 'G', 'A', 'D'],
        ['D', 'A', 'D', 'G', 'A', 'D'],
      ],
      2: [
        ['D', 'G', 'A'],
        ['D', 'A', 'D', 'G', 'A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Chcę wywyższać Imię Twe',
    chords: {
      1: [['E', 'A', 'H']],
      2: [
        ['E', 'A', 'H', 'A', 'E'],
        ['A', 'H', 'A', 'E'],
        ['A', 'H', 'gis', 'A', 'H', 'E'],
      ],
    },
    yt: '',
  },
  {
    title: 'Wszystkie narody klaskajcie w dłonie',
    chords: {
      1: [['D'], ['h'], ['G', 'A', 'D']],
    },
    yt: '',
  },
  {
    title: 'Dotknij panie moich oczu',
    chords: {
      1: [['D', 'A', 'h', 'G', 'A']],
    },
    yt: '',
  },
  {
    title: 'Jezus Chrystus moim Panem jest',
    chords: {
      1: [
        ['d', 'F', 'C', 'a', 'd'],
        ['g', 'd', 'C', 'a', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Niech będzie chwała i cześć i uwielbienie',
    chords: {
      1: [
        ['D', 'G', 'D', 'A'],
        ['D', 'G', 'D', 'A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Niech będzie chwała i cześć i uwielbienie',
    chords: {
      1: [
        ['D', 'G', 'D', 'A'],
        ['D', 'G', 'D', 'A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Jezus zwyciężył',
    chords: {
      1: [
        ['a', 'd', 'G', 'C', 'E', 'a'],
        ['a', 'd', 'E', 'a'],
      ],
    },
    yt: '',
  },
  {
    title: 'Oto Mesjasz',
    chords: {
      1: [['C', 'G', 'F', 'G', 'C']],
    },
    yt: '',
  },
  {
    title: 'Memu Bogu Królowi',
    chords: {
      1: [
        ['d', 'C', 'a', 'd', 'B', 'C', 'd'],
        ['F', 'C', 'a', 'd', 'B', 'C', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Dzielmy się wiarą jak chlebem',
    chords: {
      1: [['A', 'E', 'fis', 'D', 'E']],
      2: [
        ['fis', 'cis'],
        ['D', 'E', 'A'],
        ['D', 'E', 'A'],
      ],
    },
    yt: '',
  },
  {
    title: 'Maryjo Matko mojego wezwania',
    chords: {
      1: [['E', 'gis', 'cis', 'A']],
    },
    yt: '',
  },
  {
    title: 'Panu naszemu pieśni grajcie',
    chords: {
      1: [
        ['d', 'C', 'F', 'C', 'F', 'C', 'C', 'd', 'C'],
        ['d', 'C', 'F', 'C', 'F', 'C', 'd', 'C'],
      ],
    },
    yt: '',
  },
  {
    title: 'Nie mam nic, co mógłbym Tobie dać',
    chords: {
      1: [
        ['C', 'a', 'd'],
        ['d', 'G7', 'C'],
      ],
      2: [
        ['C', 'F', 'fis'],
        ['C', 'd', 'G', 'C'],
      ],
    },
    yt: '',
  },
  {
    title: 'Przed obliczem Pana uniżmy się',
    chords: {
      1: [
        ['d', 'C'],
        ['B', 'C', 'A', 'd', 'C', 'B'],
        ['B', 'C', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Jeden chleb',
    chords: {
      1: [
        ['D', 'G'],
        ['A', 'D'],
      ],
      2: [
        ['D', 'G'],
        ['D', 'A'],
        ['D', 'G'],
        ['A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Pan jest pasterzem moim',
    chords: {
      1: [
        ['a', 'E', 'a'],
        ['F', 'G', 'a'],
        ['F', 'G', 'a', 'E', 'a'],
        ['d', 'E', 'a', 'E', 'a'],
      ],
    },
    yt: '',
  },
  {
    title: 'Pan jest pasterzem moim',
    chords: {
      1: [
        ['a', 'E', 'a'],
        ['F', 'G', 'a'],
        ['F', 'G', 'a', 'E', 'a'],
        ['d', 'E', 'a', 'E', 'a'],
      ],
    },
    yt: '',
  },
  {
    title: 'Jam niegodzien Panie',
    chords: {
      1: [
        ['d', 'A', 'd', 'A', 'd', 'B', 'C', 'F', 'A'],
        ['d', 'A', 'd', 'A', 'd', 'B', 'C', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Witaj pokarmie',
    chords: {
      1: [
        ['d', 'a', 'B', 'C', 'd', 'a', 'B', 'C'],
        ['F', 'C', 'g', 'd', 'a', 'd', 'a', 'd'],
      ],
    },
    yt: '',
  },
  {
    title: 'Duszo ma Pana chwal',
    chords: {
      1: [
        ['F', 'C', 'G', 'a', 'F', 'C', 'G'],
        ['F', 'a', 'F', 'G', 'a', 'F', 'G', 'C'],
      ],
      2: [
        ['F', 'a', 'G', 'a', 'F', 'C', 'G', 'a'],
        ['F', 'C', 'G', 'a'],
        ['F', 'C', 'G', 'C'],
      ],
    },
    yt: '',
  },
  {
    title: 'Przyjdź jak deszcz',
    chords: {
      1: [
        ['F', 'B', 'F', 'C'],
        ['d', 'B', 'C'],
        ['F', 'B', 'F', 'C'],
        ['d', 'B', 'C'],
        ['d', 'B', 'F', 'C', 'd', 'B', 'F', 'C'],
        ['d', 'B', 'F', 'C', 'd', 'B', 'F', 'C'],
      ],
    },
    yt: '',
  },
  {
    title: 'Jesteśmy piękni',
    chords: {
      1: [
        ['D', 'A', 'h', 'G', 'A'],
        ['e', 'h', 'A'],
        ['e', 'h', 'A'],
        ['e', 'h', 'A'],
        ['e', 'h', 'A'],
        ['D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Języku ognia',
    chords: {
      1: [
        ['A', 'C', 'D', 'A'],
        ['D', 'A'],
      ],
      2: [
        ['fis', 'D', 'A'],
        ['fis', 'D', 'A'],
        ['D', 'A', 'D', 'A'],
      ],
    },
    yt: '',
  },
  {
    title: 'Hosanna, hosanna, hosanna',
    chords: {
      1: [['F', 'C', 'B', 'C']],
      2: [
        ['B', 'F', 'B', 'F', 'B', 'C', 'd'],
        ['B', 'C', 'F'],
      ],
    },
    yt: '',
  },
  {
    title: 'Zbawienie przyszło przez krzyż',
    chords: {
      1: [
        ['a', 'd', 'a'],
        ['d', 'G', 'C', 'H7', 'E'],
      ],
      2: [
        ['E', 'a', 'd', 'E'],
        ['a', 'd', 'E', 'a'],
      ],
    },
    yt: '',
  },
  {
    title: 'Chrystus wodzem',
    chords: {
      1: [
        ['D', 'A', 'h', 'fis'],
        ['G', 'A', 'fis', 'G', 'A', 'D'],
      ],
    },
    yt: '',
  },
  {
    title: 'Prawda jedyna',
    chords: {
      1: [['C', 'G', 'e', 'h']],
    },
    yt: '',
  },
  {
    title: 'Prawda jedyna',
    chords: {
      1: [['G', 'e', 'D']],
      2: [['G', 'e', 'C', 'D', 'G']],
    },
    yt: '',
  },
  {
    title: 'Jezus daje nam zbawienie',
    chords: {
      1: [['d', 'C', 'a', 'd']],
      2: [['F', 'C', 'a', 'd']],
    },
    yt: '',
  },
  {
    title: 'Zmartwychwstał Pan',
    chords: {
      1: [
        ['a', 'F', 'C', 'G'],
        ['a', 'C', 'G'],
      ],
      2: [['a', 'G', 'C', 'd', 'E7']],
    },
    yt: '',
  },
  {
    title: 'Blisko, blisko, blisko',
    chords: {
      1: [
        ['h', 'G'],
        ['e', 'Fis', 'h'],
      ],
    },
    yt: '',
  },
  {
    title: 'O Panie Tyś moim pasterzem',
    chords: {
      1: [['D', 'A', 'D', 'e', 'A', 'D']],
      2: [['G', 'A7', 'D', 'h', 'D', 'A', 'D', 'D7']],
    },
    yt: '',
  },
];

export default songs;
