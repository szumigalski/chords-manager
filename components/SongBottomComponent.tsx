import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Button, Icon, Toggle} from '@ui-kitten/components';
import {SongType} from '../types';

type SongBottomComponentProps = {
  tonNumber: number;
  setChecked: (check: boolean) => void;
  song: SongType | null | undefined;
  checked: boolean;
  setTonNumber: (check: number) => void;
};

const SongBottomComponent = ({
  song,
  setTonNumber,
  tonNumber,
  checked,
  setChecked,
}: SongBottomComponentProps) => {
  const MinusIcon = (props: any) => <Icon {...props} name="minus-outline" />;

  const PlusIcon = (props: any) => <Icon {...props} name="plus-outline" />;

  return (
    <View>
      <View style={styles.toneView}>
        <Button
          appearance="ghost"
          accessoryLeft={MinusIcon}
          size="giant"
          style={styles.toneButton}
          onPress={() => {
            setTonNumber(tonNumber - 1);
          }}
        />
        <Text style={styles.toneNumber}>{tonNumber}</Text>
        <Button
          appearance="ghost"
          size="giant"
          accessoryLeft={PlusIcon}
          style={styles.toneButton}
          onPress={() => {
            setTonNumber(tonNumber + 1);
          }}
        />
      </View>
      {song && song.yt.length ? (
        <Toggle checked={checked} onChange={setChecked} style={styles.showFilm}>
          {'Pokaż film'}
        </Toggle>
      ) : (
        <Text />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  toneView: {
    position: 'absolute',
    bottom: 35,
    right: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 5,
  },
  toneButton: {width: 15},
  toneNumber: {
    fontSize: 15,
  },
  showFilm: {
    position: 'absolute',
    bottom: 5,
    left: '50%',
  },
});

export default SongBottomComponent;
