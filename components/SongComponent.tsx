import React, {useState, useEffect} from 'react';
import YoutubePlayer from 'react-native-youtube-iframe';
import {
  Text,
  Autocomplete,
  Button,
  Card,
  Icon,
  AutocompleteItem,
} from '@ui-kitten/components';
import {View, Dimensions, StyleSheet} from 'react-native';
import {transposeTablePlus, transposeTableMinus} from '../transpone';
import {SongType, Chord} from '../types';

const StarIcon = (props: any) => (
  <Icon {...props} name="close-circle-outline" />
);

type SongComponentProps = {
  setValue: (value: string) => void;
  data: SongType[];
  tonNumber: number;
  setSong: (song: SongType | null) => void;
  song: SongType | null | undefined;
  checked: boolean;
  value: string;
};

const SongComponent = ({
  setValue,
  data,
  tonNumber,
  value,
  setSong,
  song,
  checked,
}: SongComponentProps) => {
  const [selectData, setSelectData] = useState<SongType[] | null | undefined>(
    null,
  );

  useEffect(() => {
    data && setSelectData(data);
  }, [data]);

  const onSelect = (index: number) => {
    if (selectData && selectData[index]) {
      setValue(selectData[index].title);
    }
  };

  const transposeChord = (chord: Chord | null) => {
    if (tonNumber === 0) {
      return chord;
    }
    if (tonNumber > 0) {
      for (let i = 0; i < tonNumber; i += 1) {
        if (chord) {
          chord = transposeTablePlus[chord];
        }
      }
      return chord;
    } else {
      for (let j = 0; j > tonNumber; j -= 1) {
        if (chord) {
          chord = transposeTableMinus[chord];
        }
      }
      return chord;
    }
  };

  const onChangeText = (query: string) => {
    setValue(query);
    selectData && setSelectData(selectData.filter(item => filter(item, query)));
  };

  const renderOption = (item: SongType, index: number | string) => (
    <AutocompleteItem key={index} title={item.title} />
  );

  const filter = (item: SongType, query: string) =>
    item.title.toLowerCase().includes(query.toLowerCase());

  return (
    <View>
      <View style={styles.body}>
        <Autocomplete
          placeholder="Szukaj pieśni..."
          value={value}
          style={styles.autocomplete}
          onSelect={onSelect}
          onChangeText={onChangeText}>
          {selectData && selectData.map(renderOption)}
        </Autocomplete>
        <Button
          appearance="ghost"
          accessoryLeft={StarIcon}
          style={styles.autocompleteButton}
          onPress={() => {
            setValue('');
            setSong(null);
          }}
        />
      </View>
      {song ? (
        <Card style={styles.titleCard}>
          <Text category="h6">{song.title}</Text>
        </Card>
      ) : (
        <Text />
      )}
      {song ? (
        <Card style={styles.bodyCard}>
          {Object.keys(song.chords).map(
            key =>
              song.chords[key] &&
              // @ts-ignore: Object is possibly 'null'.
              song.chords[key].length &&
              // @ts-ignore: Object is possibly 'null'.
              song.chords[key].map((wers: Chord[]) => {
                let string = '';
                wers.map((chord: Chord) => {
                  string += ' ' + transposeChord(chord);
                });
                return (
                  <Text style={styles.chords} key={Math.random()}>
                    {string}
                  </Text>
                );
              }),
          )}
        </Card>
      ) : (
        <View style={styles.chooseChordView}>
          <Card style={styles.chooseChordCard}>
            <Text>Wybierz pieśń</Text>
          </Card>
        </View>
      )}
      {checked && song && song.yt.length ? (
        <YoutubePlayer height={400} width={300} videoId={song.yt} />
      ) : (
        <Text />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    display: 'flex',
    flexDirection: 'row',
    width: Dimensions.get('window').width,
  },
  autocomplete: {
    width: 300,
  },
  autocompleteButton: {
    width: 20,
    color: 'grey',
  },
  titleCard: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    marginBottom: 5,
  },
  bodyCard: {
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
  chords: {
    fontSize: 25,
  },
  chooseChordView: {
    width: '100%',
    height: Dimensions.get('window').height - 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  chooseChordCard: {
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
});

export default SongComponent;
