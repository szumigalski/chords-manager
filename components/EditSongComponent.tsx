import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {
  Input,
  Card,
  Button,
  Icon,
  Text,
  Autocomplete,
  AutocompleteItem,
} from '@ui-kitten/components';
import {cloneDeep} from 'lodash';
import Toast from 'react-native-simple-toast';
import {SongType, Chords} from '../types';

type EditSongComponentProps = {
  songs: SongType[] | null;
  setSongs: (songs: SongType[]) => void;
  tab: number;
};

const EditSongComponent = ({songs, setSongs, tab}: EditSongComponentProps) => {
  const [selectData, setSelectData] = useState<SongType[] | null | undefined>(
    null,
  );
  const [index, setIndex] = useState<number | null>(null);
  const [title, setTitle] = useState<string | undefined>('');
  const [yt, setYt] = useState('');
  const [chords, setChords] = useState<Chords>({});

  useEffect(() => {
    songs && setSelectData(songs);
  }, [songs]);

  const TrashIcon = (props: any) => <Icon {...props} name="trash-outline" />;
  const StarIcon = (props: any) => (
    <Icon {...props} name="close-circle-outline" />
  );

  const onSelect = (index1: number) => {
    if (selectData && selectData[index1]) {
      setTitle(selectData[index1].title);
      let newChords = cloneDeep(selectData[index1].chords);
      Object.keys(selectData[index1].chords).map(key => {
        selectData[index1].chords[key].map((line, i) => {
          newChords[key][i] = line.join(' ');
        });
      });
      setChords(newChords);
      setYt(selectData[index1].yt);
      setIndex(index1);
    }
  };

  const renderOption = (item: SongType, index3: number) => (
    <AutocompleteItem key={index3} title={item.title} />
  );

  const filter = (item: SongType, query: string) =>
    item.title.toLowerCase().includes(query.toLowerCase());

  const onChangeText = (query: string) => {
    setTitle(query);
    selectData && setSelectData(selectData.filter(item => filter(item, query)));
  };

  return (
    <View>
      {tab === 2 && (
        <View style={styles.body}>
          <Autocomplete
            placeholder="Szukaj pieśni..."
            value={title}
            style={styles.autocomplete}
            onSelect={onSelect}
            onChangeText={onChangeText}>
            {selectData && selectData.map(renderOption)}
          </Autocomplete>
          <Button
            appearance="ghost"
            accessoryLeft={StarIcon}
            style={styles.autocompleteButton}
            onPress={() => {
              setTitle('');
              setYt('');
              setChords({});
            }}
          />
        </View>
      )}
      <Input
        label="Tytuł"
        placeholder="Wpisz tytuł..."
        value={title}
        onChangeText={setTitle}
      />
      <Text>Zwrotki</Text>
      {Object.keys(chords) &&
        Object.keys(chords).map((part, i) => {
          return (
            <Card key={i}>
              {chords[part] &&
                // @ts-ignore: Object is possibly 'null'.
                chords[part].length &&
                // @ts-ignore: Object is possibly 'null'.
                chords[part].map((line, j) => (
                  <View style={styles.line} key={j}>
                    <Input
                      placeholder="Wpisz akordy..."
                      value={line}
                      style={styles.lineInput}
                      onChangeText={value => {
                        const newChords = cloneDeep(chords);
                        newChords[part][j] = value;
                        setChords(newChords);
                      }}
                    />
                    <Button
                      appearance="outline"
                      size="small"
                      accessoryLeft={TrashIcon}
                      onPress={() => {
                        const newChords = cloneDeep(chords);
                        delete newChords[part];
                        setChords(newChords);
                      }}
                      status="danger"
                    />
                  </View>
                ))}
              <View style={styles.newLineButton}>
                <Button
                  status="success"
                  size="small"
                  appearance="outline"
                  onPress={() => {
                    const newChords = cloneDeep(chords);
                    newChords[part] && newChords[part].push('');
                    setChords(newChords);
                  }}>
                  Dodaj linię
                </Button>
                <Button
                  appearance="outline"
                  size="small"
                  accessoryLeft={TrashIcon}
                  onPress={() => {
                    const newChords = cloneDeep(chords);
                    delete newChords[part];
                    setChords(newChords);
                  }}
                  status="danger">
                  Usuń zwrotkę
                </Button>
              </View>
            </Card>
          );
        })}
      <Button
        status="success"
        appearance="outline"
        size="small"
        style={styles.newPartButton}
        onPress={() => {
          const newChords = cloneDeep(chords);
          newChords[Object.keys(chords).length + 1] = [''];
          setChords(newChords);
        }}>
        Dodaj zwrotkę
      </Button>
      <Input
        label="Youtube link"
        placeholder="Wpisz link do filmu..."
        value={yt}
        onChangeText={setYt}
        style={styles.yt}
      />
      <Button
        size="small"
        appearance="outline"
        onPress={() => {
          const newSongs = cloneDeep(songs);
          let newChords = cloneDeep(chords);
          Object.keys(chords) &&
            Object.keys(chords).map(key => {
              chords[key] &&
                // @ts-ignore: Object is possibly 'null'.
                chords[key].map((line, i) => {
                  if (newChords[key] && newChords[key][i]) {
                    newChords[key][i] = line.split(' ');
                  }
                });
            });
          if (tab === 2 && newSongs && index && title) {
            newSongs[index] = {
              title: title,
              chords: cloneDeep(newChords),
              yt: yt,
            };
          } else {
            newSongs &&
              title &&
              newSongs.push({
                title: title,
                chords: cloneDeep(newChords),
                yt: yt,
              });
          }
          newSongs && setSongs(newSongs);
          Toast.show(tab === 2 ? 'Zapisano pieśń' : 'Dodano pieśń');
          setTitle('');
          setYt('');
          setChords({});
        }}>
        {tab === 2 ? 'Zapisz pieśń' : 'Dodaj pieśń'}
      </Button>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    display: 'flex',
    flexDirection: 'row',
    width: Dimensions.get('window').width,
  },
  autocomplete: {
    width: 300,
  },
  autocompleteButton: {
    width: 20,
    color: 'grey',
  },
  line: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  lineInput: {
    width: '80%',
  },
  newLineButton: {flexDirection: 'row', justifyContent: 'space-between'},
  newPartButton: {marginTop: 20, marginBottom: 10},
  yt: {marginBottom: 30},
});

export default EditSongComponent;
